/** 505.10: Basic api */
//B1: import thư viện expressjs
const express = require('express')

//B2: khởi tạo app express
const app = express()

//B3: khai báo cổng chạy app
const port = 8000

//Khai báo thư viện để sử dụng json
app.use(express.json())

app.get('/', (req, res) => {
    let today = new Date();
    console.log(today);
    res.json({
        message:`Xin chào, hôm nay là ngày ${today.getDate()} tháng ${today.getMonth() + 1} năm ${today.getFullYear()}`
    })
})

/** 505.20: các phương thức api */
//get method
app.get('/get-method', (req, res) => {
    console.log('Get Method demo');
    res.status(200).json({
        message:'Get Method demo'
    })
})

//post method
app.post('/post-method', (req, res) => {
    console.log('Post Method demo');
    res.status(201).json({
        message:'Post Method demo'
    })
})

//put method
app.put('/put-method', (req, res) => {
    console.log('Put Method demo');
    res.status(200).json({
        message:'Put Method demo'
    })
})

//delete method
app.delete('/delete-method', (req, res) => {
    console.log('Delete Method demo');
    res.status(200).json({
        message:'Delete Method demo'
    })
})

/** 505.30: các tham số api */
//path variable param
app.get('/get-param/:param1/:param2', (req, res) => {
    console.log(req.params);
    let param1 = req.params.param1;
    let param2 = req.params.param2;
    res.json({
        param1,
        param2
    })
})

//query string param: /get-query?name=ABC&age=10
app.get('/get-query', (req, res) => {
    console.log(req.query);
    let query = req.query;
    res.json({
        query
    })
})

//body json param
app.post('/post-body', (req, res) => {
    console.log(req.body);
    let body = req.body;
    res.json({
        body
    })
})

//B4: start app tại cổng 8000
app.listen(port, () => {
    console.log(`Listening on port ${port}`)
})